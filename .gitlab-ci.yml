---
###############################################################################
# Define all YAML node anchors
###############################################################################
.node_anchors:
  # `only` (also used for `except` where applicable)
  only_branch_master_parent_repo: &only_branch_master_parent_repo
    - 'master@myii/ut-tweak-tool'
  # `stage`
  stage_build: &stage_build 'build'
  stage_lint: &stage_lint 'lint'
  stage_release: &stage_release 'release'
  # `image`
  image_clickable_amd64: &image_clickable_amd64 'clickable/ci-16.04-amd64'
  image_clickable_arm64: &image_clickable_arm64 'clickable/ci-16.04-arm64'
  image_clickable_armhf: &image_clickable_armhf 'clickable/ci-16.04-armhf'
  image_commitlint: &image_commitlint 'myii/ssf-commitlint:11'
  image_eslint: &image_eslint
    name: 'cytopia/eslint:latest'
    entrypoint: ['/bin/ash', '-c']
  image_semantic-release: &image_semanticrelease 'myii/ssf-semantic-release-gitlab:17.3'
  image_shellcheck: &image_shellcheck 'koalaman/shellcheck-alpine:latest'
  image_yamllint: &image_yamllint
    name: 'cytopia/yamllint:latest'
    entrypoint: ['/bin/ash', '-c']
  # `artifacts.paths`
  paths_amd64: &paths_amd64
    - 'build/x86_64-linux-gnu/app/*.click'
  paths_arm64: &paths_arm64
    - 'build/aarch64-linux-gnu/app/*.click'
  paths_armhf: &paths_armhf
    - 'build/arm-linux-gnueabihf/app/*.click'

###############################################################################
# Define stages
###############################################################################
stages:
  - *stage_lint
  - *stage_release
  - *stage_build

###############################################################################
# `lint` stage: `yamllint`, `shellcheck` & `commitlint`
###############################################################################
yamllint:
  stage: *stage_lint
  image: *image_yamllint
  script:
    - 'yamllint -s .'

shellcheck:
  stage: *stage_lint
  image: *image_shellcheck
  script:
    - 'shellcheck --version'
    - 'find . -type f \( -iname \*.sh -o -iname \*.bash -o -iname \*.ksh \)
                      | xargs shellcheck'

eslint:
  stage: *stage_lint
  image: *image_eslint
  script:
    - 'eslint .'

commitlint:
  stage: *stage_lint
  image: *image_commitlint
  script:
    # Add `upstream` remote to get access to `upstream/master`
    - 'git remote add upstream
       https://gitlab.com/myii/ut-tweak-tool.git'
    - 'git fetch --all'
    # Run `commitlint`
    - 'commitlint --from "$(git merge-base upstream/master HEAD)"
                  --to   "${CI_COMMIT_SHA}"
                  --verbose'

###############################################################################
# `release` stage: `semantic-release`
###############################################################################
semantic-release:
  only: *only_branch_master_parent_repo
  stage: *stage_release
  image: *image_semanticrelease
  script:
    - 'semantic-release'

###############################################################################
# Define `build` template
###############################################################################
.build_click:
  stage: *stage_build
  before_script:
    - |
      if [ "${CI_COMMIT_BRANCH}" = "master" ]; then
        git checkout master
      else
        echo "Append date/time to version number here..."
        VERSION=$(grep "set(VERSION" CMakeLists.txt | cut -d\" -f2)
        VERSION=${VERSION}-$(date "+%Y%m%d-%H%M%S")
        echo "${VERSION}"
        sh bin/pre-commit_semantic-release.sh ${VERSION}
      fi
  script:
    - 'grep "set(VERSION" CMakeLists.txt'
    - 'grep "readonly property var appVersion" src/app/qml/main.qml'
    - 'clickable build'
  artifacts:
    expire_in: '1 week'

###############################################################################
# `build` stage: build clicks for `armhf`, `arm64` & `amd64`
###############################################################################
xenial_armhf_click:
  extends: '.build_click'
  image: *image_clickable_armhf
  artifacts:
    paths: *paths_armhf

xenial_arm64_click:
  extends: '.build_click'
  image: *image_clickable_arm64
  artifacts:
    paths: *paths_arm64

xenial_amd64_click:
  extends: '.build_click'
  image: *image_clickable_amd64
  artifacts:
    paths: *paths_amd64
